;This file will be executed next to the application bundle image
;I.e. current directory will contain folder PasoKani with application files
[Setup]
AppId={{fxApplication}}
AppName=PasoKani
AppVersion=1.0
AppVerName=PasoKani 1.0
AppPublisher=PasoKani
AppComments=PasoKani
AppCopyright=Copyright (C) 2015 Geokido
;AppPublisherURL=http://java.com/
;AppSupportURL=http://java.com/
;AppUpdatesURL=http://java.com/
DefaultDirName={localappdata}\PasoKani
DisableStartupPrompt=Yes
DisableDirPage=No
DisableProgramGroupPage=Yes
DisableReadyPage=No
DisableFinishedPage=No
DisableWelcomePage=No
DefaultGroupName=PasoKani
;Optional License
LicenseFile=
;WinXP or above
MinVersion=0,5.1
OutputBaseFilename=PasoKani-1.0
Compression=lzma
SolidCompression=yes
PrivilegesRequired=lowest
SetupIconFile=PasoKani\PasoKani.ico
UninstallDisplayIcon={app}\PasoKani.ico
UninstallDisplayName=PasoKani
WizardImageStretch=No
WizardSmallImageFile=PasoKani-setup-icon.bmp
ArchitecturesInstallIn64BitMode=x64


[Languages]
Name: english; MessagesFile: compiler:Default.isl

[Files]
Source: PasoKani\PasoKani.exe; DestDir: {app}; Flags: ignoreversion
Source: PasoKani\*; DestDir: {app}; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: {group}\PasoKani; Filename: {app}\PasoKani.exe; IconFilename: {app}\PasoKani.ico; Check: returnTrue()
Name: {commondesktop}\PasoKani; Filename: {app}\PasoKani.exe; IconFilename: {app}\PasoKani.ico; Check: returnFalse()


[Run]
Filename: {app}\PasoKani.exe; Description: {cm:LaunchProgram,PasoKani}; Flags: nowait postinstall skipifsilent; Check: returnTrue()
Filename: {app}\PasoKani.exe; Parameters: "-install -svcName ""PasoKani"" -svcDesc ""PasoKani"" -mainExe ""PasoKani.exe""  "; Check: returnFalse()

[UninstallRun]
Filename: "{app}\PasoKani.exe "; Parameters: -uninstall -svcName PasoKani -stopOnUninstall; Check: returnFalse()

[Code]
function returnTrue(): Boolean;
begin
  Result := True;
end;

function returnFalse(): Boolean;
begin
  Result := False;
end;

function InitializeSetup(): Boolean;
begin
// Possible future improvements:
//   if version less or same => just launch app
//   if upgrade => check if same app is running and wait for it to exit
//   Add pack200/unpack200 support?
  Result := True;
end;
