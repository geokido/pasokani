package geokido.pasokani.data;

public abstract class MappableDataObject {
	
	private String key;
	public boolean retainKey;
	
	public String getKey(){
		return this.key;
	}
	
	public void setKey(String newKey){
		this.key = newKey;
	}

}
