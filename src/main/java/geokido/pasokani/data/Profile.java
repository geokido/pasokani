package geokido.pasokani.data;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Profile extends MappableDataObject {
	
	private final String username;
	private final String gravatar;
	private final int level;
	private final String title;
	
	@JsonCreator
	public Profile(
			@JsonProperty("username") String username, 
			@JsonProperty("gravatar") String gravatar, 
			@JsonProperty("level") int level, 
			@JsonProperty("title") String title
		){
		super.retainKey = true;
		this.username = username;
		this.gravatar = gravatar;
		this.level = level;
		this.title = title;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public String getGravatar(){
		return this.gravatar;
	}
	
	public int getLevel(){
		return this.level;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	@JsonAnySetter
	public void ignoreProperty(String key, Object value){
		// Auto-Ignore unknown JSON properties.
	}

}
