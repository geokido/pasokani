package geokido.pasokani.application;

public class Configuration {
	
	private static boolean isPrivateComputer = true;

	public static boolean isPrivateComputer() {
		return isPrivateComputer;
	}
	
	public static void setPrivateComputer(boolean state){
		isPrivateComputer = state;
	}

}
