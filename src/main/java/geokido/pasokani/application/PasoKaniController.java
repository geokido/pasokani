package geokido.pasokani.application;

import geokido.pasokani.data.Profile;
import geokido.pasokani.exceptions.ExceptionHandler;
import geokido.pasokani.profile.ProfileManager;
import geokido.pasokani.timers.PasoKaniTimer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;

import org.dom4j.DocumentException;

public class PasoKaniController {
	
	// GLOBAL
	@FXML private Tab profileTab;
	@FXML private Tab timersTab;
	@FXML private Tab alertManagementTab;
	@FXML private Tab settingsTab;
	@FXML private Tab aboutTab;
	
	@FXML private Label statusLabel;
	
	// PROFILE TAB
	@FXML private CheckBox privateComputer;
	
	@FXML private ChoiceBox<String> profileList;
	private Map<String,Profile> profileMap = new HashMap<String,Profile>(); 
	@FXML private Button loadButton;
	
	@FXML private TextField keyField;
	@FXML private Button startButton;
	
	// TIMERS TAB
	@FXML private Label daysLabel;
	@FXML private Label hoursLabel;
	@FXML private Label minutesLabel;
	@FXML private Label secondsLabel;
	
	// THREADS/TIMERS
	public static PasoKaniTimer nextReviewTimer;
	public static Timer applicationTimer;
		  
	@FXML
	public void initialize() {
		try{
			initGlobal();
			initProfileTab();
			initTimersTab();
		} catch (Exception e){
			ExceptionHandler.handleException(e);
		}
	}

	private void initTimersTab() {
		nextReviewTimer = new PasoKaniTimer();
		applicationTimer = new Timer();
		nextReviewTimer.setTime(1443177935L);
		applicationTimer.scheduleAtFixedRate(nextReviewTimer, 0, 1000);
		daysLabel.textProperty().bind(nextReviewTimer.days().asString());
		hoursLabel.textProperty().bindBidirectional(nextReviewTimer.hours(), nextReviewTimer.getConverter());
		minutesLabel.textProperty().bindBidirectional(nextReviewTimer.minutes(), nextReviewTimer.getConverter());
		secondsLabel.textProperty().bindBidirectional(nextReviewTimer.seconds(), nextReviewTimer.getConverter());
	}

	private void initGlobal() {
		// ...
	}

	private void initProfileTab() throws URISyntaxException, DocumentException, IOException {
		initProfileList();
	}

	private void initProfileList() throws URISyntaxException, DocumentException, IOException {
		ObservableList<String> profiles = FXCollections.observableArrayList();
		for(Profile profile: ProfileManager.getProfileList()){
			String display = "Level "+profile.getLevel()+" "+profile.getUsername()+" of sect "+profile.getTitle();
			profiles.add(display);
			profileMap.put(display, profile);
		}
		profileList.setItems(profiles);
	}

}
