package geokido.pasokani.application;

import geokido.pasokani.customization.CustomizationManager;
import geokido.pasokani.exceptions.ExceptionHandler;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class PasoKani extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/geokido/pasokani/application/PasoKani.fxml"));
			Scene scene = new Scene(root);
			if(!CustomizationManager.isCustomSkinActive()){
				scene.getStylesheets().add(getClass().getResource("/geokido/pasokani/skins/default.css").toExternalForm());
			} else {
				scene.getStylesheets().add(CustomizationManager.getPathToCustomSkin());
			}
			primaryStage.setResizable(false);
			primaryStage.setTitle("PasoKani - Your humble WaniKani computer assisstant");
			primaryStage.getIcons().add(new Image("/geokido/pasokani/images/errorIcon.png"));
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            public void handle(WindowEvent t) {
            	PasoKaniController.applicationTimer.cancel();
                Platform.exit();
            }

        });
	}
	
	

	public static void main(String[] args) {
		try{
			launch(args);
		} catch (Exception e){
			ExceptionHandler.handleException(e);
		}
	}

}
