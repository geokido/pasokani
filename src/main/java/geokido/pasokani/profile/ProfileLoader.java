package geokido.pasokani.profile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import geokido.pasokani.application.Configuration;
import geokido.pasokani.data.Profile;
import geokido.pasokani.exceptions.ProfileFileNotFoundException;

public class ProfileLoader {
	
	private static final String PROFILE_FILE = "/data/profiles.xml";
	
	public static List<Profile> loadProfiles() throws URISyntaxException, ProfileFileNotFoundException, DocumentException{
		List<Profile> profiles = new ArrayList<Profile>();
		File jarFile = new File(ProfileLoader.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		File profileFile = new File(jarFile.getParentFile().getAbsolutePath()+PROFILE_FILE);
		if(profileFile.exists()){
			SAXReader xmlReader = new SAXReader();
			Document xmlFile = xmlReader.read(profileFile);
			Element root = xmlFile.getRootElement();
			@SuppressWarnings("unchecked")
			List<Element> profileElements = root.elements();
			for(Element profile : profileElements){
				Profile profileObject = convertElementToProfile(profile);
				profiles.add(profileObject);
			}
		} else {
			throw new ProfileFileNotFoundException();
		}
		return profiles;
	}
	
	public static void saveProfiles(List<Profile> profileList) throws IOException{
		if(Configuration.isPrivateComputer()){
			Document profileXML = DocumentHelper.createDocument();
			Element root = profileXML.addElement("profiles");
			for (Profile profile : profileList){
				Element profileElement = convertProfileToElement(profile);
				root.add(profileElement);
			}
			File profileFile = new File(PROFILE_FILE);
			if(!profileFile.exists()){
				profileFile.createNewFile();
			}
			XMLWriter writer = new XMLWriter(new FileWriter(profileFile),OutputFormat.createPrettyPrint());
			writer.write(profileXML);
			writer.flush();
			writer.close();
		}
	}
	
	private static Profile convertElementToProfile(Element profileElement){
		Element nameElement = profileElement.element("name");
		String name = nameElement.getText();
		Element avatarElement = profileElement.element("gravatar");
		String avatar = avatarElement.getText();
		Element levelElement = profileElement.element("level");
		int level = Integer.parseInt(levelElement.getText());
		Element sectElement = profileElement.element("sect");
		String sect = sectElement.getText();
		Element keyElement = profileElement.element("key");
		String key = keyElement.getText();
		Profile convertedProfile = new Profile(name,avatar,level,sect);
		convertedProfile.setKey(key);
		return convertedProfile;
	}
	
	private static Element convertProfileToElement(Profile profileObject){
		Element profileElement = DocumentHelper.createElement("profile");
		Element nameElement = profileElement.addElement("name");
		nameElement.setText(profileObject.getUsername());
		Element avatarElement = profileElement.addElement("gravatar");
		avatarElement.setText(profileObject.getGravatar());
		Element levelElement = profileElement.addElement("level");
		levelElement.setText(String.valueOf(profileObject.getLevel()));
		Element sectElement = profileElement.addElement("sect");
		sectElement.setText(profileObject.getTitle());
		Element keyElement = profileElement.addElement("key");
		keyElement.setText(profileObject.getKey());
		return profileElement;
	}

}
