package geokido.pasokani.profile;

import geokido.pasokani.api.PasoKaniEngine;
import geokido.pasokani.data.Profile;
import geokido.pasokani.exceptions.NoActiveUserException;
import geokido.pasokani.exceptions.ProfileFileNotFoundException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.DocumentException;

import com.fasterxml.jackson.core.JsonProcessingException;

public class ProfileManager {
	
	private static List<Profile> profileList;
	
	private static Profile activeUser;
	
	public static String getApiKey() throws NoActiveUserException{
		if(activeUser != null){
			return activeUser.getKey();
		} else {
			throw new NoActiveUserException();
		}
	}
	
	private static void loadProfiles() throws URISyntaxException, DocumentException, IOException{
		try{
			profileList = ProfileLoader.loadProfiles();
		} catch (ProfileFileNotFoundException e){
			profileList = new ArrayList<Profile>();
			ProfileLoader.saveProfiles(profileList);
		}
	}
	
	public static List<Profile> getProfileList() throws URISyntaxException, DocumentException, IOException{
		loadProfiles();
		return profileList;
	}
	
	private static Profile findProfileFromListByKey(String apiKey) throws URISyntaxException, DocumentException, IOException{
		Profile result = null;
		if(apiKey != null){
			if(profileList == null){
				loadProfiles();
			}
			for(Profile profile : profileList){
				if(apiKey.equals(profile.getKey())){
					result = profile;
					break;
				}
			}
		}
		return result;
	}
	
	public static void updateApiKey(String oldApiKey, String newApiKey) throws URISyntaxException, DocumentException, IOException{
		if(newApiKey != null && !newApiKey.isEmpty()){
			if(profileList == null){
				loadProfiles();
			}
			Profile profile = findProfileFromListByKey(oldApiKey);
			if(profile != null){
				profile.setKey(newApiKey);
				ProfileLoader.saveProfiles(profileList);
			}
		}
	}
	
	public static void addNewProfile(String apiKey) throws JsonProcessingException, MalformedURLException, IOException, URISyntaxException, DocumentException{
		Profile newProfile = PasoKaniEngine.retrieveProfile(apiKey);
		if(profileList == null){
			profileList = new ArrayList<Profile>();
			profileList.add(newProfile);
		} else {
			Profile existingProfile = findProfileFromListByKey(apiKey);
			if(existingProfile == null){
				profileList.add(newProfile);
			}
		}
		ProfileLoader.saveProfiles(profileList);
	}
	
	public static void addProfile(Profile profile) throws IOException, URISyntaxException, DocumentException{
		if(profileList == null){
			profileList = new ArrayList<Profile>();
			profileList.add(profile);
		} else {
			Profile existingProfile = findProfileFromListByKey(profile.getKey());
			if(existingProfile == null){
				profileList.add(profile);
			}
		}
		ProfileLoader.saveProfiles(profileList);
	}

}
