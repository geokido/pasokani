package geokido.pasokani.timers;

import java.util.TimerTask;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.util.StringConverter;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

public class PasoKaniTimer extends TimerTask {
	
	private static boolean taskActive = true;
	
	private static DateTime timeStamp = new DateTime(1);
	private static DateTime oldTimeStamp = new DateTime(1);
	
	private static IntegerProperty daysRemaining = new SimpleIntegerProperty(0);
	private static IntegerProperty hoursRemaining = new SimpleIntegerProperty(0);
	private static IntegerProperty minutesRemaining = new SimpleIntegerProperty(0);
	private static IntegerProperty secondsRemaining = new SimpleIntegerProperty(0);
	
	private static StringConverter<Number> valueConverter;

	@Override
	public void run() {
		Platform.runLater(
			new Runnable() {
		       public void run() {
		    	   if(timeStamp.isAfter(new DateTime(1))){
		   			if(timeStamp.isBefore(oldTimeStamp)||timeStamp.isAfter(oldTimeStamp)){
		   				DateTime currentTime = new DateTime();
		   				int daysBetween = Days.daysBetween(currentTime, timeStamp).getDays();
		   				if(daysBetween > 0){
		   					daysRemaining.set(daysBetween);
		   				} else {
		   					daysRemaining.set(0);
		   				}
		   				int hoursBetween = Hours.hoursBetween(currentTime, timeStamp).getHours();
		   				if(hoursBetween > 0){
		   					hoursRemaining.set(hoursBetween%24);
		   				} else {
		   					hoursRemaining.set(0);
		   				}
		   				int minutesBetween = Minutes.minutesBetween(currentTime, timeStamp).getMinutes();
		   				if(minutesBetween > 0){
		   					minutesRemaining.set(minutesBetween%60);
		   				} else {
		   					minutesRemaining.set(0);
		   				}
		   				int secondsBetween = Seconds.secondsBetween(currentTime, timeStamp).getSeconds();
		   				if(secondsBetween > 0){
		   					secondsRemaining.set(secondsBetween%60);
		   				} else {
		   					secondsRemaining.set(0);
		   				}
		   				oldTimeStamp = timeStamp;
		   				taskActive = true;
		   			} else {
		   				if(taskActive){
		   					if(daysRemaining.get() == 0 && hoursRemaining.get() == 0 && minutesRemaining.get() == 0 && secondsRemaining.get() == 0){
		   						daysRemaining.set(0);
		   						hoursRemaining.set(0);
		   						minutesRemaining.set(0);
		   						secondsRemaining.set(0);
		   						taskActive = false;
		   					} else {
		   						decrementSecond();
		   					}
		   				}
		   			}
		   		}
		      }
		});
	}
	
	private boolean decrementSecond() {
		if(secondsRemaining.get() > 0){
			secondsRemaining.set(secondsRemaining.get()-1);
			return true;
		} else {
			if(decrementMinute()){
				secondsRemaining.set(59);
				return true;
			} else {
				return false;
			}
		}
	}

	private boolean decrementMinute() {
		if(minutesRemaining.get() > 0){
			minutesRemaining.set(minutesRemaining.get()-1);
			return true;
		} else {
			if(decrementHour()){
				minutesRemaining.set(59);
				return true;
			} else {
				return false;
			}
		}
	}

	private boolean decrementHour() {
		if(hoursRemaining.get() > 0){
			hoursRemaining.set(hoursRemaining.get()-1);
			return true;
		} else {
			if(decrementDay()){
				hoursRemaining.set(23);
				return true;
			} else {
				return false;
			}
		}
	}

	private boolean decrementDay() {
		if(daysRemaining.get() > 0){
			daysRemaining.set(daysRemaining.get()-1);
			return true;
		} else {
			return false;
		}
	}

	public void setTime(long target){
		timeStamp = new DateTime(unixToJava(target));
	}
	
	private long unixToJava(long unixTimestamp){
		return unixTimestamp * 1000L;
	}
	
	public IntegerProperty days(){
		return daysRemaining;
	}
	
	public IntegerProperty hours(){
		return hoursRemaining;
	}
	
	public IntegerProperty minutes(){
		return minutesRemaining;
	}
	
	public IntegerProperty seconds(){
		return secondsRemaining;
	}
	
	public StringConverter<Number> getConverter(){
		if(valueConverter == null){
			valueConverter = new StringConverter<Number>(){

				@Override
				public Number fromString(String paramString) {
					return Integer.parseInt(paramString);
				}

				@Override
				public String toString(Number number) {
					Integer value = (Integer) number;
					if(value < 10){
						return "0"+value;
					} else {
						return value.toString();
					}
				}
				
			};
		}
		return valueConverter;
	}

}
