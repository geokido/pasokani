package geokido.pasokani.api;

import geokido.pasokani.api.requests.ApiRequest;
import geokido.pasokani.data.MappableDataObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RequestExecutor<ResultType extends MappableDataObject, RequestType extends ApiRequest<ResultType>> {
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	private static final String USER_INFO_KEY = "user_information";
	private static final String MAIN_INFO_KEY = "requested_information";
	
	public ResultType executeApiRequest(RequestType request) throws JsonProcessingException, MalformedURLException, IOException{
		
		String apiKey = request.getApiKey();
		String resource = request.getResource();
		String option = request.getOption();
		boolean isMainInfoRequest = request.isMainInfoRequest();
		
		String requestUrl = URLBuilder.buildURL(resource, option, apiKey);
		
		JsonNode response = mapper.readTree(new URL(requestUrl));
		JsonParser dataBlock = null;
		
		if(isMainInfoRequest){
			dataBlock = response.get(MAIN_INFO_KEY).traverse();
		} else {
			dataBlock = response.get(USER_INFO_KEY).traverse();
		}
		
		ResultType result = mapper.readValue(dataBlock, request.getDataClass());
		
		if(result.retainKey){
			result.setKey(apiKey);
		}
		
		return result;
	}

}
