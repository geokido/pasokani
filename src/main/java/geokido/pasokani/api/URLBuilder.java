package geokido.pasokani.api;

import geokido.pasokani.exceptions.NoActiveUserException;
import geokido.pasokani.profile.ProfileManager;

public class URLBuilder {
	
	private static final String WK_ROOT = "https://www.wanikani.com/api/user/";
	private static final String SEP = "/";
	
	public static String buildURL(String resourceName){
		return buildURL(resourceName,null);
	}
	
	public static String buildURL(String resourceName, String optionalArgument){
		return buildURL(resourceName,optionalArgument,null);
	}
	
	public static String buildURL(String resourceName, String optionalArgument, String apiKey){
		if(apiKey == null){
			try {
				apiKey = ProfileManager.getApiKey();
			} catch (NoActiveUserException e) {
				throw new RuntimeException(e);
			}
		}
		String url = WK_ROOT+apiKey+SEP+resourceName;
		if(optionalArgument != null){
			url = url+SEP+optionalArgument;
		}
		return url;
	}

}
