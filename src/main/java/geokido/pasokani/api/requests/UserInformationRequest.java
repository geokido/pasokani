package geokido.pasokani.api.requests;

import geokido.pasokani.data.Profile;

public class UserInformationRequest extends ApiRequest<Profile> {
	
	private static final String RESOURCE = "user-information";

	public UserInformationRequest(String apiKey) {
		super(apiKey,RESOURCE,false,Profile.class);
	}

}
