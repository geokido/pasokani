package geokido.pasokani.api.requests;

import geokido.pasokani.data.MappableDataObject;

public abstract class ApiRequest<T extends MappableDataObject> {
	
	private final String apiKey;
	private final String resource;
	private final String option;
	private final boolean isMainInfo;
	private final Class<T> dataClass;
	
	public ApiRequest(String apiKey, String resource, Class<T> dataClass){
		this(apiKey,resource,true,dataClass);
	}
	
	public ApiRequest(String apiKey, String resource, String option, Class<T> dataClass){
		this(apiKey,resource,option,true,dataClass);
	}
	
	public ApiRequest(String apiKey, String resource, boolean isMainInfo, Class<T> dataClass){
		this(apiKey,resource,null,isMainInfo,dataClass);
	}
	
	public ApiRequest(String apiKey, String resource, String option, boolean isMainInfo, Class<T> dataClass){
		this.apiKey = apiKey;
		this.resource = resource;
		this.option = option;
		this.isMainInfo = isMainInfo;
		this.dataClass = dataClass;
	}
	
	public String getApiKey(){
		return this.apiKey;
	}
	
	public String getResource(){
		return this.resource;
	}
	
	public String getOption(){
		return this.option;
	}
	
	public boolean isMainInfoRequest(){
		return this.isMainInfo;
	}
	
	public Class<T> getDataClass(){
		return this.dataClass;
	}

}
