package geokido.pasokani.api;

import java.io.IOException;
import java.net.MalformedURLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import geokido.pasokani.api.requests.UserInformationRequest;
import geokido.pasokani.data.Profile;

public class PasoKaniEngine {
	
	public static Profile retrieveProfile(String apiKey) throws JsonProcessingException, MalformedURLException, IOException{
		Profile result = null;
		UserInformationRequest retrieveProfileRequest = new UserInformationRequest(apiKey);
		RequestExecutor<Profile,UserInformationRequest> executor = new RequestExecutor<Profile,UserInformationRequest>();
		result = executor.executeApiRequest(retrieveProfileRequest);
		return result;
	}

}
