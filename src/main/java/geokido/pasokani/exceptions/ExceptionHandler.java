package geokido.pasokani.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;


public class ExceptionHandler {
	
	public static final ImageView ERROR_ICON = new ImageView(new Image("/geokido/pasokani/images/errorIcon.png"));

	public static void handleException(Exception e) {
		Alert errorAlert = new Alert(AlertType.ERROR);
		errorAlert.setTitle("Oh no!");
		errorAlert.setGraphic(ERROR_ICON);
		errorAlert.setContentText(e.getMessage());
		errorAlert.getDialogPane().setExpandableContent(exceptionPane(e));
		errorAlert.setHeaderText("Looks like PasoKani encountered some bad stuff...");
		errorAlert.showAndWait();
		System.exit(0);
	}
	
	private static GridPane exceptionPane(Exception e){
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		String exceptionText = stringWriter.toString();

		Label label = new Label("The exception stacktrace was:");

		TextArea textArea = new TextArea(exceptionText);
		textArea.setEditable(false);
		textArea.setWrapText(true);

		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);
		
		return expContent;
	}
	
}
