package geokido.pasokani.exceptions;

public class NoActiveUserException extends Exception {
	
	private static final long serialVersionUID = 7580681001980580211L;

	public NoActiveUserException(){
		super("No active user found!");
	}

}
