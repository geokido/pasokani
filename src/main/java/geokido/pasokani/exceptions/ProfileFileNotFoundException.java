package geokido.pasokani.exceptions;

import java.io.FileNotFoundException;

public class ProfileFileNotFoundException extends FileNotFoundException {
	
	private static final long serialVersionUID = -7244579924421742250L;
	
	public ProfileFileNotFoundException(){
		super("WaniKani profile file is missing!");
	}

}
